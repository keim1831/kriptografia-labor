Name: <YOUR NAME>
SUNet: <SUNet ID>

In 1-3 sentences per section, comment on your approach to each of the parts of the assignment. What was your high-level strategy? How did you translate that into code? Did you make use of any Pythonic practices? We want you to reflect on your coding style, and how you are maximally leveraging Python's utilities.

# Caesar Cipher
I iterate over plaintext and  for each character in the plaintext is replaced by a letter some fixed number of positions down the alphabet. 
For example, with a shift of 3, D would be replaced by A, E would become B, and so on. And non-alphabetic characters will be the same in the ciphertext by encrypction, and in the plaintext by decryption.

# Vigenere Cipher
Same as in Caesar cipher, for each character in the plaintext is replaced by another character. 
I took the first letter of the message and the first letter of the key, I added their value. The result of the addition modulo 26 gives the rank of the ciphered letter.

# Scytale Cipher
I formatted the string character by character using circumference and build it into another string.
I used the slice python method to extract the appropriate letters from the plaintext and ciphertext and place it in the result.

# Railfence Cipher
For implementation I used arrays and the size be “the value of key” * “length of the string”. I iterated over "grid" row by row and eliminate the spaces between each letter in a row.
To do this, I parsed through each character in every row and appended all the characters, which are not spaces, to an initially empty list.


