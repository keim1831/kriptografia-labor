"""Assignment 1: Cryptography for CS41 Winter 2020.

Name: Kiraly Eva
SUNet: <SUNet ID>

Replace this placeholder text with a description of this module.
"""
import utils
import string
import math  
letters = string.ascii_letters
uppercase = string.ascii_uppercase
lowercase = string.ascii_lowercase
n = 3

#################
# CAESAR CIPHER #
#################

def encrypt_caesar(plaintext):
    result = ""   
    for symbol in plaintext:
        if symbol in uppercase:
            result += uppercase[(letters.find(symbol) + n) % 26]
        else:
            result += symbol
    return result


def decrypt_caesar(ciphertext):
    result = ""    
    for symbol in ciphertext:
        if symbol in uppercase:
            result += uppercase[(letters.find(symbol) - n) % 26]
        else:
            result += symbol
    return result
###################
# VIGENERE CIPHER #
###################

def encrypt_vigenere(plaintext, keyword):
    result = ""
    i = 0

    for c in plaintext:
        if c in uppercase:
            n = (uppercase.find(c) + uppercase.find(keyword[i % len(keyword)])) % 26
            result = result + uppercase[n]
        else:
            result = result + c
        i += 1    
    return result

def decrypt_vigenere(ciphertext, keyword): 
    result = ""
    i = 0
    for c in ciphertext:
        if c in uppercase:
            n = (uppercase.find(c) - uppercase.find(keyword[i % len(keyword)])) % 26
            result = result + uppercase[n]
        else:
            result = result + c
        i += 1    
    return result


########################################
# MERKLE-HELLMAN KNAPSACK CRYPTOSYSTEM #
########################################

def generate_private_key(n=8):
    """Generate a private key to use with the Merkle-Hellman Knapsack Cryptosystem.

    Following the instructions in the handout, construct the private key
    components of the MH Cryptosystem. This consists of 3 tasks:

    1. Build a superincreasing sequence `w` of length n
        Note: You can double-check that a sequence is superincreasing by using:
            `utils.is_superincreasing(seq)`
    2. Choose some integer `q` greater than the sum of all elements in `w`
    3. Discover an integer `r` between 2 and q that is coprime to `q`
        Note: You can use `utils.coprime(r, q)` for this.

    You'll also need to use the random module's `randint` function, which you
    will have to import.

    Somehow, you'll have to return all three of these values from this function!
    Can we do that in Python?!

    :param n: Bitsize of message to send (defaults to 8)
    :type n: int

    :returns: 3-tuple private key `(w, q, r)`, with `w` a n-tuple, and q and r ints.
    """
    # Your implementation here.
    raise NotImplementedError('generate_private_key is not yet implemented!')


def create_public_key(private_key):
    """Create a public key corresponding to the given private key.

    To accomplish this, you only need to build and return `beta` as described in
    the handout.

        beta = (b_1, b_2, ..., b_n) where b_i = r × w_i mod q

    Hint: this can be written in one or two lines using list comprehensions.

    :param private_key: The private key created by generate_private_key.
    :type private_key: 3-tuple `(w, q, r)`, with `w` a n-tuple, and q and r ints.

    :returns: n-tuple public key
    """
    # Your implementation here.
    raise NotImplementedError('create_public_key is not yet implemented!')


def encrypt_mh(message, public_key):
    """Encrypt an outgoing message using a public key.

    Following the outline of the handout, you will need to:
    1. Separate the message into chunks based on the size of the public key.
        In our case, that's the fixed value n = 8, corresponding to a single
        byte. In principle, we should work for any value of n, but we'll
        assert that it's fine to operate byte-by-byte.
    2. For each byte, determine its 8 bits (the `a_i`s). You can use
        `utils.byte_to_bits(byte)`.
    3. Encrypt the 8 message bits by computing
         c = sum of a_i * b_i for i = 1 to n
    4. Return a list of the encrypted ciphertexts for each chunk of the message.

    Hint: Think about using `zip` and other tools we've discussed in class.

    :param message: The message to be encrypted.
    :type message: bytes
    :param public_key: The public key of the message's recipient.
    :type public_key: n-tuple of ints

    :returns: Encrypted message bytes represented as a list of ints.
    """
    # Your implementation here.
    raise NotImplementedError('encrypt_mh is not yet implemented!')


def decrypt_mh(message, private_key):
    """Decrypt an incoming message using a private key.

    Following the outline of the handout, you will need to:
    1. Extract w, q, and r from the private key.
    2. Compute s, the modular inverse of r mod q, using the Extended Euclidean
        algorithm (implemented for you at `utils.modinv(r, q)`)
    3. For each byte-sized chunk, compute
         c' = cs (mod q)
    4. Solve the superincreasing subset sum problem using c' and w to recover
        the original plaintext byte.
    5. Reconstitute the decrypted bytes to form the original message.

    :param message: Encrypted message chunks.
    :type message: list of ints
    :param private_key: The private key of the recipient (you).
    :type private_key: 3-tuple of w, q, and r

    :returns: bytearray or str of decrypted characters
    """
    # Your implementation here.
    raise NotImplementedError('decrypt_mh is not yet implemented!')

###################
# SCYTALE CIPHER #
###################

def encrypt_scytale(plaintext, circumference):
    result = ""
    length = len(plaintext)
    for i in range(circumference):
        result += plaintext[slice(i, length, circumference)]
    return result
        
def decrypt_scytale(ciphertext, circumference):
    circumference2 = math.ceil(len(ciphertext)/circumference)
    result = ""
    length = len(ciphertext)    
    for i in range(circumference2):
        result += ciphertext[slice(i, length, circumference2)]
    return result

###################
# SCYTALE BINARY CIPHER #
###################

"""def encrypt_scytale_binary(byte_array, circumference):
    result = b""
    length = len(byte_array)
    for i in range(circumference):
        result += bytes(byte_array[slice(i, length, circumference)],'utf8')
    return result
        
def decrypt_scytale_binary(byte_array, circumference):
    circumference2 = math.ceil(len(ciphertext)/circumference)
    result = b""
    length = len(byte_array)    
    for i in range(circumference2):
        result += bytes(byte_array[slice(i, length, circumference2)],'utf8')
    return result"""

###################
# RAILFENCE CIPHER #
###################

def encrypt_railfence(plaintext, num_rails):
    rail = [""] * num_rails
    level = 0
    for c in plaintext:
        rail[level] += c
        if level >= num_rails-1:
            isAscending = False
        if level == 0:
            isAscending = True

        if isAscending:
            level += 1
        else:
            level -= 1
    result = "".join(rail)
    return result

def decrypt_railfence(ciphertext, num_rails):
    result = ""
    ciphertextLenght = len(ciphertext)
    #rail = [""] * num_rails
    rail = [[' ' for x in range(ciphertextLenght)]
                for y in range(num_rails)]

    row, col = 0, 0
    for i in range(ciphertextLenght):
        rail[row][col] = "."
        col += 1
        if row == num_rails - 1:
            isAscending = False 
        elif row == 0:
            isAscending = True
        
        if isAscending:
            row += 1
        else:
            row -= 1
    index = 0
    for i in range(num_rails):
        for j in range(ciphertextLenght):
            if rail[i][j] == "." and index < ciphertextLenght:
                rail[i][j] = ciphertext[index]
                index += 1
    
    row, col = 0, 0
    for i in range (ciphertextLenght):
        
        if rail[row][col] != ".":
            result += rail[row][col]
            col += 1

        if row == num_rails - 1:
            isAscending = False
        elif row == 0:
            isAscending = True
        
        if isAscending:
            row += 1
        else:
            row -= 1
    
    return result
